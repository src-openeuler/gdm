%global _hardened_build 1

%define libauditver 1.0.6
%define gtk3_version 2.99.2
%define pam_version 0.99.8.1-11
%define desktop_file_utils_version 0.2.90
%define nss_version 3.11.1

Name:          gdm
Epoch:         1
Version:       45.0.1
Release:       2
Summary:       The GNOME Display Manager
License:       GPLv2+
URL:           https://wiki.gnome.org/Projects/GDM
Source0:       http://download.gnome.org/sources/gdm/45/gdm-%{version}.tar.xz
Source1:       org.gnome.login-screen.gschema.override
Source5:       default.pa-for-gdm

Patch0:        0001-Honor-initial-setup-being-disabled-by-distro-install.patch
Patch1:        0001-data-add-system-dconf-databases-to-gdm-profile.patch

BuildRequires: accountsservice-devel
BuildRequires: audit-libs-devel >= %{libauditver}
BuildRequires: dconf
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: gettext-devel
BuildRequires: keyutils-libs-devel
BuildRequires: libXdmcp-devel
BuildRequires: libattr-devel
BuildRequires: libdmx-devel
BuildRequires: meson
BuildRequires: nss-devel >= %{nss_version}
BuildRequires: pam-devel >= 0:%{pam_version}
BuildRequires: pkgconfig(accountsservice) >= 0.6.3
BuildRequires: pkgconfig(check)
BuildRequires: pkgconfig(gobject-introspection-1.0)
BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(gudev-1.0)
BuildRequires: pkgconfig(iso-codes)
BuildRequires: pkgconfig(libcanberra-gtk3)
BuildRequires: pkgconfig(libselinux)
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(ply-boot-client)
BuildRequires: pkgconfig(systemd)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xau)
BuildRequires: pkgconfig(xorg-server)
BuildRequires: plymouth-devel
BuildRequires: systemd
BuildRequires: which
BuildRequires: xorg-x11-server-Xorg
BuildRequires: xorg-x11-server-devel
BuildRequires: yelp-devel
BuildRequires: yelp-tools

Requires:      accountsservice
Requires:      audit-libs >= %{libauditver}
Requires:      dconf
Requires:      gnome-keyring-pam
Requires:      gnome-session
Requires:      gnome-session-wayland-session
Requires:      gnome-settings-daemon >= 3.27.90
Requires:      gnome-shell
Requires:      iso-codes
Requires:      libXau >= 1.0.4-4
Requires:      pam >= 0:%{pam_version}
Requires:      /sbin/nologin
Requires:      setxkbmap
Requires:      systemd >= 186
Requires:      system-logos
Requires:      systemd-pam
Requires:      xhost xmodmap xrdb
Requires:      xorg-x11-xinit
Requires:      /usr/bin/dbus-run-session

Provides:      service(graphical-login) = %{name}
Provides:      gdm-libs%{?_isa} = %{epoch}:%{version}-%{release}
Provides:      gdm-plugin-smartcard = %{epoch}:%{version}-%{release}
Provides:      gdm-plugin-fingerprint = %{epoch}:%{version}-%{release}
Provides:      pulseaudio-gdm-hooks = 1:%{version}-%{release}
Obsoletes:     gdm-libs < 1:%{version}-%{release} gdm-plugin-smartcard < 1:%{version}-%{release}
Obsoletes:     gdm-plugin-fingerprint < 1:%{version}-%{release} pulseaudio-gdm-hooks < 1:%{version}-%{release}

Requires(pre):    shadow-utils
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
GDM, the GNOME Display Manager, handles authentication-related backend
functionality for logging in a user and unlocking the user's session after
it's been locked. GDM also provides functionality for initiating user-switching,
so more than one user can be logged in at the same time. It handles
graphical session registration with the system for both local and remote
sessions (in the latter case, via the XDMCP protocol).  In cases where the
session doesn't provide it's own display server, GDM can start the display
server on behalf of the session.

%package       devel
Summary:       Development files for gdm
Requires:      %{name} = %{epoch}:%{version}-%{release}
Requires:      pam-devel
Provides:      gdm-pam-extensions-devel = %{epoch}:%{version}-%{release}
Obsoletes:     gdm-pam-extensions-devel < %{epoch}:%{version}-%{release}

%description devel
The gdm-devel package contains headers and other
files needed to build custom greeters.

%prep
%autosetup -n %{name}-%{version} -p1
sed -i '/ExecStart/i ExecStartPre=\/bin\/sh -c "systemctl stop session-c*.scope"' data/gdm.service.in
sed -i "/redhat-release/i \ \ \ \ '\/etc\/%{_vendor}-release': 'redhat'," meson.build

%build
%meson -Dpam-prefix=%{_sysconfdir} \
       -Drun-dir=/run/gdm \
       -Dudev-dir=%{_udevrulesdir} \
       -Ddefault-path=/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin \
       -Dprofiling=true \
       -Dplymouth=enabled \
       -Dselinux=enabled
%meson_build

%install
install -d %{buildroot}%{_sysconfdir}/gdm/Init
install -d %{buildroot}%{_sysconfdir}/gdm/PreSession
install -d %{buildroot}%{_sysconfdir}/gdm/PostSession
install -d %{buildroot}%{_sysconfdir}/dconf/db/gdm.d/locks
install -d %{buildroot}/var/log/gdm
install -d %{buildroot}%{_datadir}/gdm/autostart/LoginWindow
install -d %{buildroot}/run/gdm

%meson_install

install -p -m644 -D %{SOURCE5} %{buildroot}%{_localstatedir}/lib/gdm/.config/pulse/default.pa
#install -p -m644 -D %{SOURCE6} %{buildroot}%{_sysusersdir}/%{name}.conf
rm -f %{buildroot}%{_sysconfdir}/pam.d/gdm
cp -a %{SOURCE1} %{buildroot}%{_datadir}/glib-2.0/schemas
rm -rf %{buildroot}/%{_prefix}/doc

(cd %{buildroot}%{_sysconfdir}/gdm; ln -sf ../X11/xinit/Xsession .)

%delete_la_and_a

%find_lang gdm --with-gnome

%pre
/usr/sbin/useradd -M -u 42 -d /var/lib/gdm -s /sbin/nologin -r gdm > /dev/null 2>&1
/usr/sbin/usermod -d /var/lib/gdm -s /sbin/nologin gdm >/dev/null 2>&1
exit 0

%post
custom=/etc/gdm/custom.conf

if [ $1 -ge 2 ] ; then
    if [ -f /usr/share/gdm/config/gdm.conf-custom ]; then
        oldconffile=/usr/share/gdm/config/gdm.conf-custom
    elif [ -f /etc/X11/gdm/gdm.conf ]; then
        oldconffile=/etc/X11/gdm/gdm.conf
    fi

    # Comment out some entries from the custom config file that may
    # have changed locations in the update.  Also move various
    # elements to their new locations.

    [ -n "$oldconffile" ] && sed \
    -e 's@^command=/usr/X11R6/bin/X@#command=/usr/bin/Xorg@' \
    -e 's@^Xnest=/usr/X11R6/bin/Xnest@#Xnest=/usr/X11R6/bin/Xnest@' \
    -e 's@^BaseXsession=/etc/X11/xdm/Xsession@#BaseXsession=/etc/X11/xinit/Xsession@' \
    -e 's@^BaseXsession=/etc/X11/gdm/Xsession@#&@' \
    -e 's@^BaseXsession=/etc/gdm/Xsession@#&@' \
    -e 's@^Greeter=/usr/bin/gdmgreeter@#Greeter=/usr/libexec/gdmgreeter@' \
    -e 's@^RemoteGreeter=/usr/bin/gdmlogin@#RemoteGreeter=/usr/libexec/gdmlogin@' \
    -e 's@^GraphicalTheme=Bluecurve@#&@' \
    -e 's@^BackgroundColor=#20305a@#&@' \
    -e 's@^DefaultPath=/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin@#&@' \
    -e 's@^RootPath=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/X11R6/bin@#&@' \
    -e 's@^HostImageDir=/usr/share/hosts/@#HostImageDir=/usr/share/pixmaps/faces/@' \
    -e 's@^LogDir=/var/log/gdm@#&@' \
    -e 's@^PostLoginScriptDir=/etc/X11/gdm/PostLogin@#&@' \
    -e 's@^PreLoginScriptDir=/etc/X11/gdm/PreLogin@#&@' \
    -e 's@^PreSessionScriptDir=/etc/X11/gdm/PreSession@#&@' \
    -e 's@^PostSessionScriptDir=/etc/X11/gdm/PostSession@#&@' \
    -e 's@^DisplayInitDir=/var/run/gdm.pid@#&@' \
    -e 's@^RebootCommand=/sbin/reboot;/sbin/shutdown -r now;/usr/sbin/shutdown -r now;/usr/bin/reboot@#&@' \
    -e 's@^HaltCommand=/sbin/poweroff;/sbin/shutdown -h now;/usr/sbin/shutdown -h now;/usr/bin/poweroff@#&@' \
    -e 's@^ServAuthDir=/var/gdm@#&@' \
    -e 's@^Greeter=/usr/bin/gdmlogin@Greeter=/usr/libexec/gdmlogin@' \
    -e 's@^RemoteGreeter=/usr/bin/gdmgreeter@RemoteGreeter=/usr/libexec/gdmgreeter@' \
    $oldconffile > $custom
fi

if [ $1 -ge 2 -a -f $custom ] && grep -q /etc/X11/gdm $custom ; then
   sed -i -e 's@/etc/X11/gdm@/etc/gdm@g' $custom
fi

if [ -f "$custom" ]; then
        sed -ie 's@^#WaylandEnable=false@WaylandEnable=false@' $custom
fi

%systemd_post gdm.service

%preun
%systemd_preun gdm.service

%postun
%systemd_postun gdm.service

%files -f gdm.lang
%doc AUTHORS NEWS README.md
%license COPYING
%dir %{_sysconfdir}/gdm
%config(noreplace) %{_sysconfdir}/gdm/custom.conf
%config %{_sysconfdir}/gdm/Init/*
%config %{_sysconfdir}/gdm/PostLogin/*
%config %{_sysconfdir}/gdm/PreSession/*
%config %{_sysconfdir}/gdm/PostSession/*
%config %{_sysconfdir}/pam.d/gdm-autologin
%config %{_sysconfdir}/pam.d/gdm-password
# not config files
%{_sysconfdir}/gdm/Xsession
%{_datadir}/gdm/gdm.schemas
%{_sysconfdir}/dbus-1/system.d/gdm.conf
%dir %{_sysconfdir}/gdm/Init
%dir %{_sysconfdir}/gdm/PreSession
%dir %{_sysconfdir}/gdm/PostSession
%dir %{_sysconfdir}/gdm/PostLogin
%dir %{_sysconfdir}/dconf/db/gdm.d
%dir %{_sysconfdir}/dconf/db/gdm.d/locks
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.override
%{_libexecdir}/gdm-host-chooser
%{_libexecdir}/gdm-runtime-config
%{_libexecdir}/gdm-session-worker
%{_libexecdir}/gdm-simple-chooser
%{_libexecdir}/gdm-wayland-session
%{_libexecdir}/gdm-x-session
%{_sbindir}/gdm
%{_bindir}/gdmflexiserver
%{_bindir}/gdm-screenshot
%dir %{_datadir}/dconf
%dir %{_datadir}/dconf/profile
%{_datadir}/dconf/profile/gdm
%dir %{_datadir}/gdm/greeter
%dir %{_datadir}/gdm/greeter/applications
%{_datadir}/gdm/greeter/applications/*
%dir %{_datadir}/gdm/greeter/autostart
%{_datadir}/gdm/greeter/autostart/*
%{_datadir}/gdm/greeter-dconf-defaults
%{_datadir}/gdm/locale.alias
%{_datadir}/gdm/gdb-cmd
%{_datadir}/gnome-session/sessions/gnome-login.session
%{_libdir}/girepository-1.0/Gdm-1.0.typelib
%{_libdir}/security/pam_gdm.so
%{_libdir}/libgdm*.so*
%dir %{_localstatedir}/log/gdm
%attr(1770, gdm, gdm) %dir %{_localstatedir}/lib/gdm
%attr(0700, gdm, gdm) %dir %{_localstatedir}/lib/gdm/.config
%attr(0700, gdm, gdm) %dir %{_localstatedir}/lib/gdm/.config/pulse
%attr(0600, gdm, gdm) %{_localstatedir}/lib/gdm/.config/pulse/default.pa
%attr(0711, root, gdm) %dir /run/gdm
%config %{_sysconfdir}/pam.d/gdm-smartcard
%config %{_sysconfdir}/pam.d/gdm-fingerprint
%{_sysconfdir}/pam.d/gdm-launch-environment
%{_udevrulesdir}/61-gdm.rules
%{_unitdir}/gdm.service
%dir %{_userunitdir}/gnome-session@gnome-login.target.d/
%{_userunitdir}/gnome-session@gnome-login.target.d/session.conf
#%%{_sysusersdir}/%{name}.conf

%files devel
%dir %{_includedir}/gdm
%{_includedir}/gdm/*.h
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Gdm-1.0.gir
%{_libdir}/pkgconfig/gdm.pc
%{_libdir}/pkgconfig/gdm-pam-extensions.pc

%changelog
* Tue Jan 21 2025 zhangshaoning <zhangshaoning@uniontech.com> - 1:45.0.1-2
- Fix bad date in changelog

* Thu Mar 28 2024 liweigang <liweiganga@uniontech.com> - 1:45.0.1-1
- update to version 45.0.1

* Wed Mar 13 2024 panchenbo <panchenbo@kylinsec.com.cn> - 1:43.0-2
- modify openEuler to %{_vendor}

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1:43.0-1
- Update to 43.0

* Mon Aug 1 2022 caodongxia <caodongxia@h-partners.com> - 1:42.0-2
- The installation dependency pam is added to solve the gdm.servic startup problem

* Fri Apr 15 2022 dillon chen <dillon.chen@gmail.com> - 1:42.0-1
- Update to 42.0

* Tue Aug 17 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-3
- Add 9000-add-openeuler-pam-config.patch for openeuler pam config to normally start gdm

* Wed Jun 23 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-2
- Add Omitted Sources1 and Sources5
- Add Omitted patches Patch0 and Patch1
- Recover Patch2

* Mon May 24 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Update Version, Release, Source0, BuildRequires
- Use meson rebuild and install
- Delete patches, delete pam.d related and other two files which not in 3.38.2
- Add new file session.conf. modify /etc/gdm/custom.conf

* Fri Feb 5 2021 wangxiao <wangxiao65@huawei.com> - 1:3.30.1-10
- fix CVE-2019-3825

* Fri Dec 18 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1:3.30.1-9
- Gdm should recover automatically when killed

* Thu Nov 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:3.30.1-8
- fix CVE-2020-16125

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:3.30.1-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC: delete the isa in obsoletes

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:3.30.1-6
- delete unused patch

* Mon Dec 30 2019 Ling Yang <lingyang2@huawei.com> - 1:3.30.1-5
- Disable wayland

* Tue Dec 17 2019 Jiangping Hu <hujiangping@huawei.com> - 1:3.30.1-4
- Add openEuler PAM config

* Sat Nov 23 2019 Jiangping Hu <hujiangping@huawei.com> - 1:3.30.1-3
- Package init
